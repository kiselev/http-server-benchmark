
DELAY=5

CONTAINER=benchmark-tmp
docker_cleanup() {
    docker rm -f $CONTAINER 2>/dev/null >/dev/null || true
}

benchmark() {
    ip=$1
    wrk -t4 -c50 -d10s http://$ip:80/
}

test_docker_server() {
    title="$1" ; shift
    image="$1" ; shift
    cmd="$@"
    docker run -d --name $CONTAINER $image $cmd > /dev/null
    sleep $DELAY
    ip=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' $CONTAINER)
    echo "------- Benchmark $title"
    benchmark $ip
    docker_cleanup
}

docker_cleanup

test_docker_server "nginx" kisel/nginx-dummy
test_docker_server "nxweb" nexoft/nxweb
test_docker_server "lxwan" benchmark/lxwan

test_docker_server "node simple" benchmark/node-servers node node-server.js
test_docker_server "node express" benchmark/node-servers node express-server.js
test_docker_server "node simple cluster" benchmark/node-servers node node-server-cluster.js
test_docker_server "node express cluster" benchmark/node-servers node express-server-cluster.js

